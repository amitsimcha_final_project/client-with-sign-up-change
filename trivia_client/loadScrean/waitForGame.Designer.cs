﻿namespace loadScrean
{
    partial class waitForGame
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.connactionInfoLabel = new System.Windows.Forms.Label();
            this.userNameLable = new System.Windows.Forms.Label();
            this.roomNameLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Orange;
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Location = new System.Drawing.Point(27, 63);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(534, 151);
            this.label1.TabIndex = 0;
            // 
            // connactionInfoLabel
            // 
            this.connactionInfoLabel.AutoEllipsis = true;
            this.connactionInfoLabel.AutoSize = true;
            this.connactionInfoLabel.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.connactionInfoLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.connactionInfoLabel.Location = new System.Drawing.Point(274, 81);
            this.connactionInfoLabel.Name = "connactionInfoLabel";
            this.connactionInfoLabel.Size = new System.Drawing.Size(52, 17);
            this.connactionInfoLabel.TabIndex = 1;
            this.connactionInfoLabel.Text = "label2";
            this.connactionInfoLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // userNameLable
            // 
            this.userNameLable.AutoSize = true;
            this.userNameLable.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.userNameLable.Location = new System.Drawing.Point(53, 24);
            this.userNameLable.Name = "userNameLable";
            this.userNameLable.Size = new System.Drawing.Size(46, 18);
            this.userNameLable.TabIndex = 2;
            this.userNameLable.Text = "label2";
            // 
            // roomNameLabel
            // 
            this.roomNameLabel.AutoSize = true;
            this.roomNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.roomNameLabel.Location = new System.Drawing.Point(491, 24);
            this.roomNameLabel.Name = "roomNameLabel";
            this.roomNameLabel.Size = new System.Drawing.Size(46, 18);
            this.roomNameLabel.TabIndex = 3;
            this.roomNameLabel.Text = "label3";
            // 
            // waitForGame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::loadScrean.Properties.Resources.big_clock;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(610, 483);
            this.Controls.Add(this.roomNameLabel);
            this.Controls.Add(this.userNameLable);
            this.Controls.Add(this.connactionInfoLabel);
            this.Controls.Add(this.label1);
            this.Name = "waitForGame";
            this.Text = "Wait For Game";
            this.Load += new System.EventHandler(this.waitForGame_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label connactionInfoLabel;
        private System.Windows.Forms.Label userNameLable;
        private System.Windows.Forms.Label roomNameLabel;
    }
}