﻿using System;
using System.Net.Sockets;
using System.Speech.Synthesis;
using System.Windows.Forms;

namespace loadScrean
{
    public partial class createRoom : Form
    {
        private NetworkStream _clientStream;

        private string _username;
        private bool _isAdmin;
        private SpeechSynthesizer synthesizer = new SpeechSynthesizer();

        public createRoom(NetworkStream clientStream, string username, bool isAdmin)
        {
            _clientStream = clientStream;
            _username = username;
            _isAdmin = isAdmin;

            InitializeComponent();

            UserNameLabel.Text = _username;
        }

        private void GoBackButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            this.Close();
        }

        private void SendButton_Click(object sender, EventArgs e)
        {
            string roomName = "";
            string roomNameLen = "";
            char numOfPlayers = '0';
            string numOfQuestions = "";
            string timeForQuestion = "";
            string msgType = "213";
            string fullMsgToServer = "";

            int n;
 
            if ((RoomNameTextBox.Text == "") || (NumOfPlayersTextBox.Text == "") ||
               (NumOfQuestionsTextBox.Text == "") || (TimeForQuestionTextBox.Text == ""))
            {
                ErrorLabel.Text = "Somthing is missing";
            }
            else if (false == int.TryParse(NumOfPlayersTextBox.Text.ToString(), out n) || !(Convert.ToInt32(NumOfPlayersTextBox.Text.ToString()) > 0 && Convert.ToInt32(NumOfPlayersTextBox.Text.ToString()) < 10))
            {
                ErrorLabel.Text = "invalid value of players number!";
            }
            else if (false == int.TryParse(NumOfQuestionsTextBox.Text.ToString(), out n) || !(Convert.ToInt32(NumOfQuestionsTextBox.Text.ToString()) < 100 && Convert.ToInt32(NumOfQuestionsTextBox.Text.ToString()) > 0))
            {
                ErrorLabel.Text = "invalid value of question number!";
            }
            else if (false == int.TryParse(TimeForQuestionTextBox.Text.ToString(), out n) || !(Convert.ToInt32(TimeForQuestionTextBox.Text.ToString()) > 0 && Convert.ToInt32(TimeForQuestionTextBox.Text.ToString()) < 121))
            {
                ErrorLabel.Text = "invalid value of second time!";
            }
            else
            {
                ErrorLabel.Text = ""; // to reset the error lable

                roomNameLen = (roomName.Length).ToString();
                roomName = RoomNameTextBox.Text;
                numOfPlayers = (NumOfPlayersTextBox.Text.ToCharArray())[0];
                numOfQuestions = NumOfQuestionsTextBox.Text;
                timeForQuestion = TimeForQuestionTextBox.Text;

                fullMsgToServer = msgType + roomNameLen + roomName + numOfPlayers
                                  + numOfQuestions + timeForQuestion;

                /* // send create new room message to the server
                 bufferSet = new ASCIIEncoding().GetBytes(fullMsgToServer);
                 _clientStream.Write(bufferSet, 0, bufferSet.Length);
                 _clientStream.Flush();

                 // get the answer from the server 
                 bufferGet = new byte[4];
                 int bytesRead = _clientStream.Read(bufferGet, 0, 4);
                 string ansFromServer = new ASCIIEncoding().GetString(bufferGet);

                 if(ansFromServer == "1140")
                 {
                     RoomNameTextBox.Text = "";
                     NumOfPlayersTextBox.Text= "";
                     NumOfQuestionsTextBox.Text = "";
                     TimeForQuestionTextBox.Text = "";

                     // sound setings for the speaking
                     synthesizer.Volume = 100; // 0...100
                     synthesizer.Rate = 1; // -10...10

                     synthesizer.Speak("The room was successfully created!");*/


                    // create a new wait for game window
                    waitForGame newWaitForGame_window = new waitForGame(_clientStream, _username, _isAdmin, roomName, 
                                                                        numOfPlayers.ToString(), numOfQuestions, timeForQuestion);

                    try
                    {
                        this.Hide();

                        newWaitForGame_window.ShowDialog();
                    }
                    catch (Exception) { }

                    this.Show();
               // }
                /*else if(ansFromServer == "1141")
                {
                    ErrorLabel.Text = "The room creation has faild";
                }*/
            }
        }

        private void createRoom_Load(object sender, EventArgs e)
        {

        }
    }
}
