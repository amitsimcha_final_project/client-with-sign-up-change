﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.Net.Sockets;


namespace loadScrean
{
    public partial class joinRoom : Form
    {
        private List<Tuple<int, string>> _idAndNameOfRooms;
        private int _listSize;
        private bool _isAdmin;

        private byte[] _bufferGet;
        private byte[] _bufferSet;
        NetworkStream _clientStream;

        public joinRoom(NetworkStream clientStream, string username, bool isAdmin)
        {
            _clientStream = clientStream;
            _isAdmin = isAdmin;

            InitializeComponent();

            // init the screen.
            currUsernameLabel.Text = username;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            this.Close();
        }

        private void joinRoom_Load(object sender, EventArgs e)
        {
            // send rooms list message to the server (205).
            _bufferSet = new ASCIIEncoding().GetBytes("205");
            _clientStream.Write(_bufferSet, 0, _bufferSet.Length);
            _clientStream.Flush();

            // get the type answer from the server.
            _bufferGet = new byte[3];
            int bytesRead1 = _clientStream.Read(_bufferGet, 0, 3);
            string typeMessage = new ASCIIEncoding().GetString(_bufferGet);

            // get the number of the rooms from the server.
            _bufferGet = new byte[4];
            int bytesRead2 = _clientStream.Read(_bufferGet, 0, 4);
            string numberOfRooms = new ASCIIEncoding().GetString(_bufferGet);

            // put the room id and the room name into list of tuple.
            _idAndNameOfRooms = new List<Tuple<int, string>>();
            for (int i = Convert.ToInt32(numberOfRooms); i > 0; i--)
            {
                // get the room id
                _bufferGet = new byte[4];
                int bytesRead3 = _clientStream.Read(_bufferGet, 0, 4);
                string roomId = new ASCIIEncoding().GetString(_bufferGet);

                // get the length of room's name
                _bufferGet = new byte[2];
                int bytesRead4 = _clientStream.Read(_bufferGet, 0, 2);
                string roomNameLen = new ASCIIEncoding().GetString(_bufferGet);

                // get the name of room
                _bufferGet = new byte[Convert.ToInt32(roomNameLen)];
                int bytesRead5 = _clientStream.Read(_bufferGet, 0, Convert.ToInt32(roomNameLen));
                string roomName = new ASCIIEncoding().GetString(_bufferGet);

                // add the room's parameters into a list.
                Tuple<int, string> newTuple = new Tuple<int, string>(Convert.ToInt32(roomId), roomName);
                _idAndNameOfRooms.Add(newTuple);
            }

            _listSize = Convert.ToInt32(numberOfRooms);


            if (_idAndNameOfRooms.Count == 0)
            {
                noRoomsLabel.Visible = true;
            }
            else
            {
                noRoomsLabel.Visible = false;
                for (int i = 0; i < _listSize; i++)
                {
                    Label roomName = new Label();
                    roomName.Name = "roomName" + i; // we need to new the place in the list from i.
                    roomName.Text = _idAndNameOfRooms[i].Item2.ToString();
                    roomName.Location = new Point(5, roomsListPanel.Controls.Count * 30);
                    roomName.AutoSize = true;
                    roomName.Font = new Font("Monotype Hadassah", 20);

                    roomName.Click += (sender2, e2) => seePlayersInRoom(sender, e, Convert.ToInt32(roomName.Name[roomName.Name.Length - 1]) - 48);

                    roomsListPanel.Controls.Add(roomName);
                }
            }
        }

        private void seePlayersInRoom(object sender, EventArgs e, int placeInListOfRooms)
        {
            usersInRoomPanel.Controls.Clear();
            joinButton.Enabled = true;
            usersInRoomLabel.Text = ":הם \"" + _idAndNameOfRooms[placeInListOfRooms].Item2 + "\"" + " השחקנים בחדר";
            usersInRoomLabel.Visible = true;
            usersInRoomPanel.Visible = true;


            // bulid the message of players in room (207).
            string messageToServer = "207";
            messageToServer += _idAndNameOfRooms[placeInListOfRooms].Item1.ToString("D4"); // padded the number (detsimal and 4 digits).

            // send the message.
            _bufferSet = new ASCIIEncoding().GetBytes(messageToServer);
            _clientStream.Write(_bufferSet, 0, _bufferSet.Length);
            _clientStream.Flush();

            // get the type answer from the server.
            _bufferGet = new byte[3];
            int bytesRead1 = _clientStream.Read(_bufferGet, 0, 3);
            string typeMessage = new ASCIIEncoding().GetString(_bufferGet);

            // get the number of players in the room.
            _bufferGet = new byte[1];
            int bytesRead2 = _clientStream.Read(_bufferGet, 0, 1);
            string numOfPlayers = new ASCIIEncoding().GetString(_bufferGet);

            List<string> playersInRoom = new List<string>();
            for (int i = Convert.ToInt32(numOfPlayers); i > 0; i--)
            {
                // get the length of playerName
                _bufferGet = new byte[2];
                int bytesRead3 = _clientStream.Read(_bufferGet, 0, 2);
                string playerNameLen = new ASCIIEncoding().GetString(_bufferGet);

                // get the name of player
                _bufferGet = new byte[Convert.ToInt32(playerNameLen)];
                int bytesRead5 = _clientStream.Read(_bufferGet, 0, Convert.ToInt32(playerNameLen));
                string playerName = new ASCIIEncoding().GetString(_bufferGet);

                // add the player to the panel
                Label playerNameLabel = new Label();
                playerNameLabel.Text = playerName;
                playerNameLabel.Location = new Point(5, usersInRoomPanel.Controls.Count * 30);
                playerNameLabel.AutoSize = true;
                playerNameLabel.Font = new Font("Monotype Hadassah", 20);
                usersInRoomPanel.Controls.Add(playerNameLabel);
            }


        }

        
        private void refreshButton_Click_1(object sender, EventArgs e)
        {
            usersInRoomPanel.Controls.Clear(); // clear the panel of player in room.
            roomsListPanel.Controls.Clear(); // clear the panel of the room.

            usersInRoomLabel.Visible = false; // dissapear the label of players in room.
            usersInRoomPanel.Visible = false; // dissapear the panel of players in room.

            joinButton.Enabled = false; // frees the button of join.

            _idAndNameOfRooms.Clear(); // clear the list of the rooms, because it init again.

            // start all from the begining of join room window.
            joinRoom_Load(sender, e);
        }

        private void joinButton_Click(object sender, EventArgs e)
        {
            // create a new wait for game window
            waitForGame newWaitForGame_window = new waitForGame(_clientStream,"",false,"","","","");

            try
            {
                this.Hide();

                newWaitForGame_window.ShowDialog();
            }
            catch (Exception) { }

            this.Show();
        }
    }
}
