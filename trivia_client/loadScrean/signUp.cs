﻿using System;
using System.Net.Sockets;
using System.Speech.Synthesis;
using System.Text;
using System.Windows.Forms;

using System.Threading;

namespace loadScrean
{
    public partial class SignUp : Form
    {
        private byte[] _bufferGet;
        private byte[] _bufferSet;
        private NetworkStream _clientStream;
        private SpeechSynthesizer synthesizer = new SpeechSynthesizer();

        public SignUp(NetworkStream clientStream)
        {
            _clientStream = clientStream;
            InitializeComponent();

            GoBackButton.Enabled = false; // frees the button of go back.
            OkButton.Enabled = false; // frees the button of ok.

            // sound setings for the speaking
            synthesizer.Volume = 100; // 0...100
            synthesizer.Rate = 1; // -10...10

            // going to function that say hello to the new user.
            Thread hello = new Thread(helloUser);
            hello.Start();
        }

        private void helloUser()
        {
            synthesizer.Speak("Hello, you are going to sign up to our data base." +
               " Please enter your username, password and email,, After that, click ok!");
            Invoke((MethodInvoker)delegate { GoBackButton.Enabled = true; }); // free the button of go back.
            Invoke((MethodInvoker)delegate { OkButton.Enabled = true; }); // free the button of ok.
        }

        private void GoBackButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            this.Close();
        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            string userNameLen = "";
            string passwordLen = "";
            string emailLen = "";

            if ((UserNameTextBox.Text == "") || (PasswordTextBox.Text == "") || (EmailTextBox.Text == ""))
            {
                ErrorLabel.Text = "Somthing is missing";
            }
            else
            {
                ErrorLabel.Text = ""; // to reset the error lable

                userNameLen = UserNameTextBox.Text.Length.ToString();
                if (userNameLen.Length < 2)
                {
                    userNameLen = "0" + UserNameTextBox.Text.Length;
                }

                passwordLen = PasswordTextBox.Text.Length.ToString();
                if (passwordLen.Length < 2)
                {
                    passwordLen = "0" + PasswordTextBox.Text.Length;
                }

                emailLen = EmailTextBox.Text.Length.ToString();
                if (emailLen.Length < 2)
                {
                    emailLen = "0" + EmailTextBox.Text.Length;
                }

                // create the message to the server
                string msgToServer = "203" + userNameLen + UserNameTextBox.Text + passwordLen + PasswordTextBox.Text + emailLen + EmailTextBox.Text;

                // send the data about the user to the server
                _bufferSet = new ASCIIEncoding().GetBytes(msgToServer);
                _clientStream.Write(_bufferSet, 0, msgToServer.Length);
                _clientStream.Flush();

                // get a return value from the server
                _bufferGet = new byte[4];
                int bytesRead = _clientStream.Read(_bufferGet, 0, 4);
                string answerFromServer = new ASCIIEncoding().GetString(_bufferGet);

                if (answerFromServer == "1040")
                {
                    synthesizer.Speak("Welcome aboard, " + UserNameTextBox.Text + ", you can connect to the game now!");

                    UserNameTextBox.Text = "";
                    PasswordTextBox.Text = "";
                    EmailTextBox.Text = "";

                    this.Hide();
                    this.Close();
                }
                else if (answerFromServer == "1041")
                {
                    ErrorLabel.Text = "Pass illegal";
                }
                else if (answerFromServer == "1042")
                {
                    ErrorLabel.Text = "Username is already exists";
                }
                else if (answerFromServer == "1043")
                {
                    ErrorLabel.Text = "Username is illegal";
                }
                else if (answerFromServer == "1044")
                {
                    ErrorLabel.Text = "Other";
                }
            }
        }

        private void SignUp_Load(object sender, EventArgs e)
        {

        }
    }
}
