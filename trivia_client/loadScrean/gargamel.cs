﻿using System;
using System.Speech.Synthesis;
using System.Windows.Forms;

namespace loadScrean
{
    public partial class gargamel : Form
    {
        private System.Media.SoundPlayer creepy_sound = new System.Media.SoundPlayer
                                                                        (loadScrean.Properties.Resources.creepy_sound); 

        public gargamel()
        {
            InitializeComponent();

            creepy_sound.Play();
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            creepy_sound.Stop();

            this.Hide();
            this.Close();
        }

        private void gargamel_Load(object sender, EventArgs e)
        {

        }
    }
}
